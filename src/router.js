import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from './page/HomePage.vue'

Vue.use(VueRouter)

const routes = [
    
    { 
        path: '/', 
        name: 'homePage', 
        component: HomePage ,
    },
    { 
        path: '*', 
        component: HomePage 
    },
]

export default new VueRouter({
    mode: 'history',
    routes,    
})